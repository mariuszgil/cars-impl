README
==================================

# Setup #

Aby uruchomić wersję init projektu:

```
docker-compose up -d
```

Instalacja potrzebnych bibliotek z poziomu hosta:

```
cd app
composer install
```

lub analogicznie z poziomu kontenera php-fpm, za pomocą dostępnego tam composera.

Aby zweryfikować poprawność komunikacji pomiędzy usługami należy uruchomić endpoint:

```
http://localhost:11000/test
```

Implementacja modelu z wykorzystaniem technik Domain-Driven Design według dostarczonych wytycznych, usług aplikacyjntych oraz komunikacja pomiędzy nimi odbędzie się podczas warsztatu. 

# Usługi #

Dostępne usługi:

- nginx
- php-fpm
- mysql (user: workshop, pass: workshop, db: workshop)
- rabbitmq (user: guest, pass: guest)

Definicje mapowania portów znajdują się w pliku docker-compose.yml.