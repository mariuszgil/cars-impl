<?php

namespace App\Auctions\Catalog;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AuctionDescription
{
    private UuidInterface $id;
    private string $title;
    private string $description;

    /**
     * @param UuidInterface $id
     * @param string $title
     * @param string $description
     */
    public function __construct(UuidInterface $id, string $title, string $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}