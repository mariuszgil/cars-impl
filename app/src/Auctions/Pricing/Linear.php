<?php

namespace App\Auctions\Pricing;

use Money\Money;

class Linear implements Calculator
{
    public function __construct(
        private readonly int $a,
        private readonly int $b,
    )
    {

    }

    public function calculate(int $amount): Money
    {
        return new Money($this->a * $amount + $this->b, new Currency('PLN'));
    }
}