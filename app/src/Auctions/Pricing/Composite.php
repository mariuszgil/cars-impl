<?php

namespace App\Auctions\Pricing;

use Money\Money;

class Composite implements Calculator
{
    public function __construct(
        private readonly array $steps = []
    )
    {

    }

    public function calculate(int $amount): Money
    {
        foreach ($this->steps as $step) {
            if ($step->contains($amount)) {
                 return $this->steps->calculator->calculate($amount);
            }
        }

        return Ups :)
    }
}