<?php

namespace App\Auctions\Pricing;

use Money\Money;

interface Calculator
{
    public function calculate(int $amount): Money;
}