<?php

namespace App\Auctions\Pricing;

use Money\Money;

class MoreMoneeeeeyyyy implements Calculator
{
    public function __construct(
        private readonly  Calculator $calculator
    )
    {

    }

    public function calculate(int $amount): Money
    {
        return $this->calculator->calculate($amount);
    }

}