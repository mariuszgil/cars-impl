<?php

namespace App\Auctions\Pricing;

class Step
{
    private int $left;

    private int $right;

    private Calculator $calculator;

    /**
     * @param int $left
     * @param int $right
     * @param Calculator $calculator
     */
    public function __construct(int $left, int $right, Calculator $calculator)
    {
        $this->left = $left;
        $this->right = $right;
        $this->calculator = $calculator;
    }


}