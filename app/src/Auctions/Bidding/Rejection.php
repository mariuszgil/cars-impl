<?php

namespace App\Auctions\Bidding;

class Rejection
{
    public function __construct(
        public readonly string $reason
    )
    {

    }
}