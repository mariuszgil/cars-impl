<?php

namespace App\Auctions\Bidding;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Version;
use Money\Money;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

#[Entity]
class Auctioneer
{
    #[Id]
    #[Column(type: 'uuid', unique: true)]
    private UuidInterface $id;

    #[Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $opening;

    #[Column(type: 'money')]
    private Money $askingPrice;

    #[Column(type: 'money')]
    private Money $minimalRaise;

    #[Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $closing;

    #[Column(type: 'boolean')]
    private bool $finalized = false;

    #[Column(type: 'uuid', nullable: true)]
    private ?UuidInterface $leadingOfferId = null;

    #[Column(type: 'money', nullable: true)]
    private ?Money $leadingOfferAmount = null;

    #[Column(type: 'money', nullable: true)]
    private ?Money $buyNowPrice;

    #[Column(type: 'boolean')]
    private bool $extraTime;

    // private array $biddersIds = [];

    #[Column(type: 'integer')]
    #[Version]
    protected int $version;

    /**
     * @param UuidInterface $id
     * @param \DateTimeImmutable $opening
     * @param \DateTimeImmutable $closing
     * @param Money $askingPrice
     * @param Money $minimalRaise
     */
    public function __construct(
        UuidInterface      $id,
        \DateTimeImmutable $opening,
        \DateTimeImmutable $closing,
        Money              $askingPrice,
        Money              $minimalRaise,
        ?Money             $buyNowPrice = null,
        bool               $extraTime = false
    )
    {
        $this->minimalRaise = $minimalRaise;
        $this->askingPrice = $askingPrice;
        $this->opening = $opening;
        $this->id = $id;
        $this->closing = $closing;
        $this->buyNowPrice = $buyNowPrice;
        $this->extraTime = $extraTime;
    }

    public function submitOffer(Offer $offer): AcceptedOffer|Rejection
    {
        // Blocking changes
        if (!$offer->amount->isSameCurrency($this->askingPrice)) {
            return new Rejection('Offer must be in the same currency as auction');
        }

        if ($offer->submittedAt < $this->opening ||
            $offer->submittedAt > $this->closing ||
            $this->finalized
        ) {
            return new Rejection('Offer must be submitted when auction is open');
        }

        if ($this->leadingOfferId === null) {
            if ($offer->amount < $this->askingPrice) {
                return new Rejection('First offer amount must equal asking price');
            }
        }

        if ($this->buyNowPrice === null &&
            //if ($this->hasFeature(Feature::STANDARD_BIDDING) &&
            $this->leadingOfferId !== null &&
            $offer->amount < $this->leadingOfferAmount->add($this->minimalRaise)
        ) {
            return new Rejection('Next offer amount must equal minimal raise');
        }

        // State changes:
        // a) Extending auction time when offer was submitted in last 30 minutes
        if ($this->extraTime &&
            $offer->submittedAt >= $this->closing->sub(\DateInterval::createFromDateString('30 minutes'))
        ) {
            $this->closing = $this->closing->add(\DateInterval::createFromDateString('30 seconds'));
        }

        // b) Closing auction when offer beats buy now price
        if ($this->buyNowPrice !== null && $offer->amount >= $this->buyNowPrice) {
            $this->finalized = true;
        }

        // c) Changing leading offer
        $acceptedOffer = new AcceptedOffer(
            Uuid::uuid4(),
            $this->id,
            $offer->bidderId,
            $offer->amount,
            $offer->submittedAt
        );

        $this->leadingOfferId = $acceptedOffer->getId();
        $this->leadingOfferAmount = $acceptedOffer->getAmount();

        return $acceptedOffer;
    }

    public function enableBuyNow(Money $buyNowPrice): ?Rejection
    {
        $now = new \DateTimeImmutable(); // @todo Parametrize method

        if ($now > $this->closing || $this->finalized
        ) {
            return new Rejection('Offer must be submitted when auction is open');
        }

        if ($this->leadingOfferId !== null) {
            return new Rejection('At least one offer was submitted');
        }

        $minimum = $this->askingPrice->add($this->minimalRaise);

        if (!$buyNowPrice->greaterThanOrEqual($minimum)) {
            return new Rejection('Buy now price is too low');
        }

        $this->buyNowPrice = $buyNowPrice;

        return null;
    }

    public function disableBuyNow(): ?Rejection
    {
        $now = new \DateTimeImmutable(); // @todo Parametrize method

        if ($now > $this->closing || $this->finalized
        ) {
            return new Rejection('Offer must be submitted when auction is open');
        }

        if ($this->leadingOfferId !== null) {
            return new Rejection('At least one offer was submitted');
        }

        $this->buyNowPrice = null;

        return null;
    }

    public function finalize(): void
    {
        $this->finalized = true;
    }

    public function isLeading(AcceptedOffer $offer): bool
    {
        return $this->leadingOfferId == $offer->getId();
    }

    /**
     * @return Money
     */
    public function getAskingPrice(): Money
    {
        return $this->askingPrice;
    }

    /**
     * @return Money
     */
    public function getMinimalRaise(): Money
    {
        return $this->minimalRaise;
    }
}