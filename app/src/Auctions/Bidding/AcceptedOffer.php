<?php

namespace App\Auctions\Bidding;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Embedded;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Version;
use Money\Money;
use Ramsey\Uuid\UuidInterface;

#[Entity]
class AcceptedOffer
{
    #[Id]
    #[Column(type: 'uuid', unique: true)]
    private UuidInterface $id;

    #[Column(type: 'uuid')]
    private UuidInterface $auctioneerId;

    #[Column(type: 'uuid')]
    private UuidInterface $bidderId;

    #[Column(type: 'money')]
    private Money $amount;

    #[Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $acceptedAt;

    #[Column(type: 'integer')]
    #[Version]
    protected int $version;

    /**
     * @param UuidInterface $id
     * @param UuidInterface $bidderId
     * @param Money $amount
     * @param \DateTimeImmutable $acceptedAt
     */
    public function __construct(
        UuidInterface      $id,
        UuidInterface      $auctioneerId,
        UuidInterface      $bidderId,
        Money              $amount,
        \DateTimeImmutable $acceptedAt
    )
    {
        $this->id = $id;
        $this->auctioneerId = $auctioneerId;
        $this->bidderId = $bidderId;
        $this->amount = $amount;
        $this->acceptedAt = $acceptedAt;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return UuidInterface
     */
    public function getAuctioneerId(): UuidInterface
    {
        return $this->auctioneerId;
    }

    /**
     * @return UuidInterface
     */
    public function getBidderId(): UuidInterface
    {
        return $this->bidderId;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getAcceptedAt(): \DateTimeImmutable
    {
        return $this->acceptedAt;
    }
}