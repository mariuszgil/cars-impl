<?php

namespace App\Auctions\Bidding;

use App\Auctions\Offernts\BiddingAllowance;

// $auctioneer->submitOffer($offer, $allowance);

class BiddingService
{
    public function submitOffer(Auctioneer $auctioneer, Offer $offer, BiddingAllowance $allowance): AcceptedOffer|Rejection
    {
        if ($allowance->isActive()) {
            return $auctioneer->submitOffer($offer);
        }

        return new Rejection('Not allowed to submit an offer');
    }
}