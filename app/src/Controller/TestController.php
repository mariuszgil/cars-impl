<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    #[Route('/test')]
    public function test(Connection $connection): Response
    {
        $dbTime = $connection->executeQuery('SELECT NOW()')->fetchOne();

        $this->sendMessageWithoutThinkingAboutSoftwareEngineeringAndBestPractices();

        return new Response(
            'DB time: ' . $dbTime
        );
    }

    private function sendMessageWithoutThinkingAboutSoftwareEngineeringAndBestPractices()
    {
        $exchange = 'router';
        $queue = 'msgs';

        $connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest', '/');
        $channel = $connection->channel();

        $channel->queue_declare($queue, false, true, false, false);
        $channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, true, false);
        $channel->queue_bind($queue, $exchange);

        $messageBody = (new \DateTimeImmutable())->format('Y-m-d H:i:s');
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $res = $channel->basic_publish($message, $exchange);

        $channel->close();
        $connection->close();
    }
}