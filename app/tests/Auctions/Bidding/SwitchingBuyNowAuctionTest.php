<?php

namespace App\Tests\Auctions\Bidding;

use App\Auctions\Bidding\Rejection;
use Money\Money;
use PHPUnit\Framework\TestCase;

class SwitchingBuyNowAuctionTest extends TestCase
{
    private AuctioneerFactory $factory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factory = new AuctioneerFactory();
    }

    /**
     * @test
     */
    public function offerWithBuyNowPriceEndsAnAuction(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer();
        $buyNowPrice = $auctioneer->getAskingPrice()->multiply(2);

        $auctioneer->enableBuyNow($buyNowPrice);
        $auctioneer->submitOffer(
            $this->factory->offer($buyNowPrice)
        );

        // when
        $result = $auctioneer->submitOffer(
            $this->factory->offer($buyNowPrice->multiply(2))
        );

        // then
        $this->assertInstanceOf(Rejection::class, $result);
    }

    /**
     * @test
     */
    public function buyNowCannotBeEnabledWhenAtLeastOneOfferWasSubmitted(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer();

        $auctioneer->submitOffer(
            $this->factory->offer($auctioneer->getAskingPrice())
        );

        // when
        $result = $auctioneer->enableBuyNow($auctioneer->getAskingPrice()->multiply(2));

        // then
        $this->assertInstanceOf(Rejection::class, $result);
    }

    /**
     * @test
     */
    public function buyNowCannotBeDisabledWhenAtLeastOneOfferWasSubmitted(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer();

        $auctioneer->enableBuyNow($auctioneer->getAskingPrice()->multiply(2));
        $auctioneer->submitOffer(
            $this->factory->offer($auctioneer->getAskingPrice())
        );

        // when
        $result = $auctioneer->disableBuyNow();

        // then
        $this->assertInstanceOf(Rejection::class, $result);
    }

    /**
     * @test
     */
    public function buyNowPriceMustBeProperlyDefined(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer();

        // when
        $result = $auctioneer->enableBuyNow(Money::EUR(109));

        // then
        $this->assertInstanceOf(Rejection::class, $result);
    }
}