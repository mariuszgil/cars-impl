<?php

namespace App\Tests\Auctions\Bidding;

use App\Auctions\Bidding\AcceptedOffer;
use App\Auctions\Bidding\Auctioneer;
use App\Auctions\Bidding\Offer;
use App\Auctions\Bidding\Rejection;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class SubmittingAuctionOfferTest extends TestCase
{
    private AuctioneerFactory $factory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factory = new AuctioneerFactory();
    }

    /**
     * @test
     */
    public function biddingStartsWithAnInitialOffer(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer();

        // when
        $acceptedOffer = $auctioneer->submitOffer(
            $this->factory->offer($auctioneer->getAskingPrice())
        );

        // then
        $this->assertInstanceOf(AcceptedOffer::class, $acceptedOffer);
        $this->assertTrue($auctioneer->isLeading($acceptedOffer));
    }

    /**
     * @test
     */
    public function betterOfferOutbidCurrentlyLeadingOffer(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer();
        $firstAcceptedOffer = $auctioneer->submitOffer(
            $this->factory->offer($auctioneer->getAskingPrice())
        );

        // when
        $secondAcceptedOffer = $auctioneer->submitOffer(
            $this->factory->offer(
                $firstAcceptedOffer->getAmount()->add($auctioneer->getMinimalRaise())
            )
        );

        // then
        $this->assertInstanceOf(AcceptedOffer::class, $secondAcceptedOffer);
        $this->assertTrue($auctioneer->isLeading($secondAcceptedOffer));
        $this->assertFalse($auctioneer->isLeading($firstAcceptedOffer));
    }

    /**
     * @test
     * @dataProvider offersToReject
     */
    public function someOffersMustBeRejected(Auctioneer $auctioneer, Offer $offer): void
    {
        // when & then
        $this->assertInstanceOf(Rejection::class, $auctioneer->submitOffer($offer));
    }

    public static function offersToReject(): array
    {
        $factory = new AuctioneerFactory();

        return [
            'first offer must equal the asking price' => [
                $factory->auctioneer(),
                $factory->offer(Money::EUR(99)),
            ],
            'next offer must equal the minimal raise' => [
                $factory->auctioneerWithOffers(),
                $factory->offer(Money::EUR(9)),
            ],
            'offer cannot be submitted before opening' => [
                $factory->auctioneer('+1 day', '+2 days'),
                $factory->offer(Money::EUR(200)),
            ],
            'offer cannot be submitted after closing' => [
                $factory->auctioneer('-2 days', '-1 day'),
                $factory->offer(Money::EUR(200)),
            ],
            'offer cannot be submitted after finalization' => [
                $factory->terminatedAuctioneer(),
                $factory->offer(Money::EUR(200)),
            ],
            'offer cannot be submitted in different currency' => [
                $factory->auctioneer('-1 day', '+1 day', 100, 10, 'USD'),
                $factory->offer(Money::EUR(200)),
            ],
        ];
    }

    /**
     * @test
     */
    public function lastMinuteBiddingExtendsAuctionTime(): void
    {
        // given
        $auctioneer = $this->factory->auctioneer(
            '-1 day',
            '+30 minutes',
            100,
            10,
            'EUR',
            null,
            true
        );
        $auctioneer->submitOffer($this->factory->offer(Money::EUR(200)));

        // when
        $offerAfterInitialClosing = $auctioneer->submitOffer(new Offer(
            Uuid::uuid4(),
            Money::EUR(300),
            (new \DateTimeImmutable())->add(\DateInterval::createFromDateString('30 minutes 29 seconds'))
        ));

        // then
        $this->assertTrue($auctioneer->isLeading($offerAfterInitialClosing));
    }
}