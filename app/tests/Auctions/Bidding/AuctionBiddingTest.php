<?php

namespace App\Tests\Auctions\Bidding;

use App\Auctions\Bidding\AcceptedOffer;
use App\Auctions\Bidding\Auctioneer;
use App\Auctions\Bidding\Offer;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class AuctionBiddingTest extends TestCase
{
    public function testOfferCanBeAccepted(): void
    {
        // given
        $auctioneer = new Auctioneer(
            Uuid::uuid4(),
            new \DateTimeImmutable('-1 day'),
            new \DateTimeImmutable('+1 day'),
            Money::EUR(100),
            Money::EUR(10),
        );

        // when
        $acceptedOffed = $auctioneer->submitOffer(new Offer(Uuid::uuid4(), Money::EUR(300), new \DateTimeImmutable('now')));

        // then
        $this->assertInstanceOf(AcceptedOffer::class, $acceptedOffed);
    }

    public function testSubmittedOfferCanOutbidExistingOne(): void
    {
        // given
        $auctioneer = new Auctioneer(
            Uuid::uuid4(),
            new \DateTimeImmutable('-1 day'),
            new \DateTimeImmutable('+1 day'),
            Money::EUR(100),
            Money::EUR(10),
        );

        $first = $auctioneer->submitOffer(new Offer(Uuid::uuid4(), Money::EUR(200), new \DateTimeImmutable('now')));

        // when
        $second = $auctioneer->submitOffer(new Offer(Uuid::uuid4(), Money::EUR(300), new \DateTimeImmutable('now')));

        // then
        $this->assertFalse($auctioneer->isLeading($first));
        $this->assertTrue($auctioneer->isLeading($second));
    }
}