<?php

namespace App\Tests\Auctions\Bidding;

use App\Auctions\Bidding\Auctioneer;
use App\Auctions\Bidding\Offer;
use Money\Currency;
use Money\Money;
use Ramsey\Uuid\Uuid;

class AuctioneerFactory
{
    public function offer(Money $amount): Offer
    {
        return new Offer(
            Uuid::uuid4(),
            $amount,
            new \DateTimeImmutable()
        );
    }

    public function auctioneer(
        string $opening = '-1 day',
        string $closing = '+1 day',
        int    $askingPrice = 100,
        int    $minimalRaise = 10,
        string $currency = 'EUR',
        int    $buyNowPrice = null,
        bool   $extraTime = false
    ): Auctioneer
    {
        return new Auctioneer(
            Uuid::uuid4(),
            new \DateTimeImmutable($opening),
            new \DateTimeImmutable($closing),
            new Money($askingPrice, new Currency($currency)),
            new Money($minimalRaise, new Currency($currency)),
            $buyNowPrice,
            $extraTime
        );
    }

    public function terminatedAuctioneer(): Auctioneer
    {
        $auctioneer = $this->auctioneer();

        $auctioneer->finalize();

        return $auctioneer;
    }

    public function auctioneerWithOffers(): Auctioneer
    {
        $auctioneer = $this->auctioneer();

        $auctioneer->submitOffer($this->offer($auctioneer->getAskingPrice()));
        $auctioneer->submitOffer($this->offer($auctioneer->getAskingPrice()->add($auctioneer->getMinimalRaise())));

        return $auctioneer;
    }
}