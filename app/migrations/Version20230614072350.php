<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230614072350 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE accepted_offer (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', auctioneer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', bidder_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', amount VARCHAR(255) NOT NULL COMMENT \'(DC2Type:money)\', accepted_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', version INT DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auctioneer (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', opening DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', asking_price VARCHAR(255) NOT NULL COMMENT \'(DC2Type:money)\', minimal_raise VARCHAR(255) NOT NULL COMMENT \'(DC2Type:money)\', closing DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', finalized TINYINT(1) NOT NULL, leading_offer_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', leading_offer_amount VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:money)\', buy_now_price VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:money)\', extra_time TINYINT(1) NOT NULL, version INT DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE accepted_offer');
        $this->addSql('DROP TABLE auctioneer');
    }
}
